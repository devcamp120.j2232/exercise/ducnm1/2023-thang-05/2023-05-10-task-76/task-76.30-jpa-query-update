package com.devcamp.pizza365.repository;

import com.devcamp.pizza365.model.CCountry;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryRepository extends JpaRepository<CCountry, Long> {
	CCountry findByCountryCode(String countryCode);

	@Query(value = "select * from country where country_name like :countryName", nativeQuery = true)
	public Optional< List<CCountry>> findCountryByCountryNameLike(@Param("countryName") String countryName, Pageable pageable);
}
